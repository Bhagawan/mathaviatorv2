package com.example.mathaviatorv2

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.mathaviatorv2.game.GameState
import com.example.mathaviatorv2.util.Question
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    private val _gameRestart = MutableSharedFlow<Boolean>()
    val gameRestart: SharedFlow<Boolean> = _gameRestart

    private val _gameIntro = MutableStateFlow(true)
    val gameIntro: StateFlow<Boolean> = _gameIntro

    private val _gameEnd = MutableSharedFlow<Boolean>(1)
    val gameEnd: SharedFlow<Boolean> = _gameEnd

    private val _gameUp = MutableSharedFlow<Boolean>()
    val gameUp: SharedFlow<Boolean> = _gameUp

    private val _score = MutableStateFlow("0")
    val score: StateFlow<String> = _score

    private val _question = MutableStateFlow<Question?>(null)
    val question: StateFlow<Question?> = _question
    private var questionTimer : Job? = null

    private val _gameState = MutableStateFlow<GameState?>(null)
    val gameState: StateFlow<GameState?> = _gameState

    init {
        viewModelScope.launch {
            _score.emit("0")
        }
    }

    fun restart() {
        if(questionTimer?.isActive == true) questionTimer?.cancel()
        viewModelScope.launch {
            _score.emit("0")
            _gameRestart.emit(true)
            _question.emit(Question())
            _gameEnd.emit(false)
            if(_gameIntro.value) _gameIntro.emit(false)
        }
    }

    fun crash() {
        viewModelScope.launch {
            _gameEnd.emit(true)
            _question.emit(null)
        }
    }

    fun updateScore(score: Int) {
        viewModelScope.launch { _score.emit("$score м") }
    }

    fun answer(answer: String) {
        _question.value?.let {
            if(answer == it.correct) {
                viewModelScope.launch {
                    _question.emit(null)
                    _gameUp.emit(true)
                }
            }
        }
        startTimer()
    }

    fun saveGameState(state: GameState) = viewModelScope.launch { _gameState.emit(state) }

    private fun startTimer() {
        questionTimer = viewModelScope.launch {
            delay(3000)
            _question.emit(Question())
        }
    }

}