package com.example.mathaviatorv2.game

import androidx.annotation.Keep

@Keep
data class GameState(val planeX : Float, val planeY: Float, val planeAngle: Float, val dX: Float, val dY: Float,val up: Boolean, val state: Int, val gameInterface: AviatorGameView.GameInterface?)
