package com.example.mathaviatorv2.util

import androidx.annotation.Keep
import kotlin.random.Random

@Keep
class Question {
    val question: String
    val a1: String
    val a2: String
    val a3: String
    val correct: String

    init {
        val a = Random.nextInt(2,10)
        val b = Random.nextInt(2,10)
        correct = (a * b).toString()

        question = "$a * $b = "

        val c = Random.nextInt(3)
        a1 = if(c == 0) (a * b).toString()
        else ((a + 1 * Random.nextInt(-1,2)) * (b + 1 * Random.nextInt(-1,2))).toString()
        a2 = if(c == 1) (a * b).toString()
        else ((a + 1 * Random.nextInt(-1,2)) * (b + 1 * Random.nextInt(-1,2))).toString()
        a3 = if(c == 2) (a * b).toString()
        else ((a + 1 * Random.nextInt(-1,2)) * (b + 1 * Random.nextInt(-1,2))).toString()

    }
}