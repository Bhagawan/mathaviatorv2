package com.example.mathaviatorv2.util

import androidx.annotation.Keep

@Keep
data class MathAviatorV2SplashResponse(val url : String)