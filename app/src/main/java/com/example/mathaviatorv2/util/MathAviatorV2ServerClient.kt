package com.example.mathaviatorv2.util

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface MathAviatorV2ServerClient {

    @FormUrlEncoded
    @POST("MathAviatorV2/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String
                  , @Field("id") id: String ): Response<MathAviatorV2SplashResponse>

    companion object {
        fun create() : MathAviatorV2ServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(MathAviatorV2ServerClient::class.java)
        }
    }

}