package com.example.mathaviatorv2

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.PopupWindow
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.example.mathaviatorv2.databinding.ActivityMainBinding
import com.example.mathaviatorv2.databinding.PopupGameEndBinding
import com.example.mathaviatorv2.databinding.PopupGameIntroBinding
import com.example.mathaviatorv2.game.AviatorGameView
import com.example.mathaviatorv2.util.Question
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MainViewModel
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        viewModel = ViewModelProvider(this)[MainViewModel::class.java]
        setContentView(binding.root)


        lifecycleScope.launch {
            viewModel.gameRestart.collect { if(it) binding.game.restart()}
        }

        lifecycleScope.launch {
            viewModel.gameUp.collect { if(it) binding.game.up() }
        }

        lifecycleScope.launch {
            viewModel.score.collect { binding.textViewPoints.text = it }
        }

        binding.game.setGameInterface(object : AviatorGameView.GameInterface {
            override fun updateScore(score: Int) {
                viewModel.updateScore(score)
            }
            override fun crash() {
                viewModel.crash()
            }
        })

        binding.btnQuestion1.setOnClickListener {
            viewModel.answer(binding.btnQuestion1.text.toString())
            hideQuestion()
        }
        binding.btnQuestion2.setOnClickListener {
            viewModel.answer(binding.btnQuestion2.text.toString())
            hideQuestion()
        }
        binding.btnQuestion3.setOnClickListener {
            viewModel.answer(binding.btnQuestion3.text.toString())
            hideQuestion()
        }

        binding.root.post {
            lifecycleScope.launch {
                viewModel.gameIntro.collect { if(it) showIntro() }
            }
            lifecycleScope.launch {
                viewModel.gameEnd.collect { if(it) showEndPopup(viewModel.score.value) }
            }
            lifecycleScope.launch {
                viewModel.question.collect { if(it != null) showQuestion(it) else hideQuestion() }
            }
        }
        setBackground()
    }

    override fun onResume() {
        val gameState = viewModel.gameState.value
        if(gameState != null) binding.game.setGameState(gameState)
        super.onResume()
    }

    override fun onPause() {
        viewModel.saveGameState(binding.game.getGameState())
        super.onPause()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showIntro() {
        val popupBinding: PopupGameIntroBinding = PopupGameIntroBinding.inflate(layoutInflater, binding.root,false)

        val width = (binding.root.width / 1.3f).toInt()
        val height = binding.root.height / 2

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        popupBinding.root.setOnClickListener {
            popupWindow.dismiss()
            viewModel.restart()
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true

        popupWindow.animationStyle = R.style.PopupAnimation

        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showEndPopup(distance: String) {
        hideQuestion()
        val popupBinding: PopupGameEndBinding = PopupGameEndBinding.inflate(layoutInflater, binding.root,false)

        val width = (binding.root.width / 1.3f).toInt()
        val height = if(binding.root.width > binding.root.height) (binding.root.height * 0.7f).toInt() else (binding.root.height * 0.5f).toInt()

        popupBinding.popupEndDistance.text = distance
        val popupWindow = PopupWindow(popupBinding.root, width, height, true)

        popupBinding.root.setOnClickListener {
            viewModel.restart()
            popupWindow.dismiss()
        }

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true

        popupWindow.animationStyle = R.style.PopupAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
    }

    private fun hideQuestion() {
        binding.layoutQuestion.visibility = View.INVISIBLE
    }

    private fun showQuestion(question: Question) {
        binding.layoutQuestion.visibility = View.VISIBLE
        binding.question.text = question.question
        binding.btnQuestion1.text = question.a1
        binding.btnQuestion2.text = question.a2
        binding.btnQuestion3.text = question.a3
    }

    private fun setBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom) {
                bitmap?.let { binding.root.background = BitmapDrawable(resources, bitmap) }
            }

            override fun onBitmapFailed(e: Exception, errorDrawable: Drawable?) {}
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {}
        }
        Picasso.get().load("http://195.201.125.8/MathAviatorV2/back.png").into(target)
    }
}